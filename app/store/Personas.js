Ext.define('Ejemplo2Edit.store.Personas', {
    extend: 'Ext.data.Store',
    requires: [
    	'Ejemplo2Edit.model.Persona'
    ],
	config:{
		storeId : 'mgrPersonaId',
	    model: 'Ejemplo2Edit.model.Persona',
        autoSync: true,
        autoLoad: true,
	    proxy:{
            type:'ajax',
            url: 'http://localhost/sencha/c1.core/services/buscapersonas.php',
            reader:{
                type:'json',
                rootProperty:'data'
            }
        }
	}
});