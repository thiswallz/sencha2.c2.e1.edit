Ext.define('Ejemplo2Edit.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Lista',
                iconCls: 'action',
                items: [
                    {
                        xtype: 'personas'               
                    }
                ]
            }
        ]
    }
});
