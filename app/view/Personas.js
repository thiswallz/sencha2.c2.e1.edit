Ext.define('Ejemplo2Edit.view.Personas', {
    extend: 'Ext.Panel',
    alias: "widget.personas",    
    requires: [
        'Ext.TitleBar', 
        'Ext.dataview.List',
        'Ext.data.proxy.Ajax',
        'Ext.data.Store',
        'Ext.util.DelayedTask'
    ],
    onEditarTap: function(){
        var me = this;
        var record = me.down('#listaId').getSelection()[0];
        if(!record){
            Ext.Msg.alert('Lista', "Selecciona un registro");
            return;
        }
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('seleccionarPersona', me, record);
        });
        //pequeño delay para simular un loading
        task.delay(500); 
    },
    config: {
        listeners: [{
            delegate: '#editarId',
            event: 'tap',
            fn: 'onEditarTap'
        }],
        width: '100%',
        height: '100%',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [
            {
                xtype: 'button',
                ui: 'action',
                text:'Editar',
                itemId: 'editarId'                
            },
            { xtype: "spacer" },
            {
                xtype: 'button',
                iconMask: true,
                iconCls: 'refresh',
                ui: 'confirm',
                action: 'refresh',
                itemId: 'refreshId',
                handler: function() {
                    var st = Ext.StoreMgr.get('mgrPersonaId');
                    st.load();
                }
            }]
        },{
            xtype: 'list',
            itemId: 'listaId',
            itemTpl: '{id}: {nombre}',
            width: '100%',
            height: '100%',
            padding: 30,
            pinHeaders: false,
            centered: true,
            store: 'mgrPersonaId',
            loadingText: "Cargando...",
            masked: true
        }]
    }
});
