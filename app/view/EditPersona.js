Ext.define('Ejemplo2Edit.view.EditPersona', {
    extend: 'Ext.form.Panel',
    alias: "widget.editpersona",    
    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.form.Number',
        'Ext.form.Hidden', 
        'Ext.Label'    
    ],
    initialize: function() {
        this.callParent();
        var me = this;
        this.down('#nombreId').setValue(me.recordForm.nombre)
        this.down('#idPersonaId').setValue(me.recordForm.id)

    },
    config: {
        fullscreen : true,        
        layout: 'vbox',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [{
                xtype: 'button',
                ui: 'back',
                action: 'btnVolver',
                text:'Volver',
                itemId: 'volverId'
            }]
        },{
            xtype: 'fieldset',
            title: 'Persona',
            flex: 1,
            items: [{
                xtype: 'hiddenfield',
                name:'id',
                itemId: 'idPersonaId'
            },{
                label : 'Nombre',
                xtype: 'textfield',
                itemId: 'nombreId',
                name: 'nombre'              
            }]
        },{           
            xtype:'container',
            flex : 2,
            layout : {
                type : 'hbox',
                align: 'center'
            },
            items :[{
                xtype: 'button',
                ui : 'confirm',
                text: 'Editar',
                width: '90%',
                margin: 7,
                handler: function(){
                    var form = this.up('editpersona')
                    form.submit({
                        url: 'http://localhost/sencha/c1.core/services/yyyy.php',
                        method: 'POST',
                        success: function() {
                            Ext.Msg.alert('Info', "Ingresado Correctamente");
                        },
                        failure: function(proxy, response) {
                            var ob = response;
                            Ext.Msg.alert('Alerta', ob.mensaje)
                        }
                    });
                }                    
                }]
            }         
        ]
    }
});
