Ext.define('Ejemplo2Edit.controller.Personas', {
    extend: 'Ext.app.Controller',
    //load stores
    config: {
        refs: {
            personas: 'personas'        
        },
        control: {
            'button[action=btnVolver]': {
                tap: 'volverLista'
            },
            personas: {
                seleccionarPersona: 'seleccionarPersona'
            }
        }
    },
    launch: function() {
        //console.log(this);
    },
    volverLista: function(){
        Ext.Viewport.animateActiveItem(this.getPersonas(), {type: 'slide', direction: 'right'});
    },
    seleccionarPersona: function(view, record){
        var recordForm = {
            nombre: record.get('nombre'),
            id: record.get('id')        
        }
        Ext.Viewport.animateActiveItem({xtype : 'editpersona', recordForm: recordForm}, {type: 'slide', direction: 'left'});
    }
});