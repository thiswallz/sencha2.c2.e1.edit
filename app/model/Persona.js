Ext.define('Ejemplo2Edit.model.Persona', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'id', type: 'auto'},
            {name: 'nombre', type: 'auto'},
            {name: 'edad', type:'auto'},
            {name: 'mail', type:'auto'},
			{name: 'img', type:'auto'}
        ],
        validations: [
            {type: 'presence',  field: 'nombre', message : "Requerido"},
            {type: 'format', name: 'mail', matcher: /\w+@{1}\w+\.{1}\w+/, message : " no Valido"}        ]    
    }
});
